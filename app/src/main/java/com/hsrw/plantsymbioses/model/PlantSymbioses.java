package com.hsrw.plantsymbioses.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PlantSymbioses implements Parcelable {
    private int id;
    private String name;
    private String image;
    private String description;

    public PlantSymbioses() {
    }

    protected PlantSymbioses(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readString();
        description = in.readString();
    }

    public static final Creator<PlantSymbioses> CREATOR = new Creator<PlantSymbioses>() {
        @Override
        public PlantSymbioses createFromParcel(Parcel in) {
            return new PlantSymbioses(in);
        }

        @Override
        public PlantSymbioses[] newArray(int size) {
            return new PlantSymbioses[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(image);
        parcel.writeString(description);

    }
}
