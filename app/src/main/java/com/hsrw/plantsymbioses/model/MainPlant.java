package com.hsrw.plantsymbioses.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class MainPlant implements Parcelable {
    private int id;
    private String name;
    private String description;
    private String maxProduction;
    private String favorableLight;
    private String soilType;
    private String image;
    private List<PlantSymbioses> mutualisticList;
    private List<PlantSymbioses> parasiticList;
    private List<PlantSymbioses> competitionList;
    private List<PlantSymbioses> commensalisticList;

    public MainPlant() {
    }


    protected MainPlant(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        maxProduction = in.readString();
        favorableLight = in.readString();
        soilType = in.readString();
        image = in.readString();
        mutualisticList = in.createTypedArrayList(PlantSymbioses.CREATOR);
        parasiticList = in.createTypedArrayList(PlantSymbioses.CREATOR);
        competitionList = in.createTypedArrayList(PlantSymbioses.CREATOR);
        commensalisticList = in.createTypedArrayList(PlantSymbioses.CREATOR);

    }


    public static final Creator<MainPlant> CREATOR = new Creator<MainPlant>() {
        @Override
        public MainPlant createFromParcel(Parcel in) {
            return new MainPlant(in);
        }

        @Override
        public MainPlant[] newArray(int size) {
            return new MainPlant[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(maxProduction);
        parcel.writeString(favorableLight);
        parcel.writeString(soilType);
        parcel.writeString(image);
        parcel.writeTypedList(mutualisticList);
        parcel.writeTypedList(parasiticList);
        parcel.writeTypedList(competitionList);
        parcel.writeTypedList(commensalisticList);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMaxProduction() {
        return maxProduction;
    }

    public void setMaxProduction(String maxProduction) {
        this.maxProduction = maxProduction;
    }

    public String getFavorableLight() {
        return favorableLight;
    }

    public void setFavorableLight(String favorableLight) {
        this.favorableLight = favorableLight;
    }

    public String getSoilType() {
        return soilType;
    }

    public void setSoilType(String soilType) {
        this.soilType = soilType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<PlantSymbioses> getMutualisticList() {
        if (mutualisticList == null) {
            mutualisticList = new ArrayList<>();
            return mutualisticList;
        }
        return mutualisticList;
    }

    public void setMutualisticList(List<PlantSymbioses> mutualisticList) {
        this.mutualisticList = mutualisticList;
    }

    public List<PlantSymbioses> getParasiticList() {
        if (parasiticList == null) {
            parasiticList = new ArrayList<>();
            return parasiticList;
        }
        return parasiticList;
    }

    public void setParasiticList(List<PlantSymbioses> parasiticList) {
        this.parasiticList = parasiticList;
    }

    public List<PlantSymbioses> getCompetitionList() {
        if (competitionList == null) {
            competitionList = new ArrayList<>();
            return competitionList;
        }
        return competitionList;
    }

    public void setCompetitionList(List<PlantSymbioses> competitionList) {
        this.competitionList = competitionList;
    }

    public List<PlantSymbioses> getCommensalisticList() {
        if (commensalisticList == null) {
            commensalisticList = new ArrayList<>();
            return commensalisticList;
        }
        return commensalisticList;
    }

    public void setCommensalisticList(List<PlantSymbioses> commensalisticList) {
        this.commensalisticList = commensalisticList;
    }


}
