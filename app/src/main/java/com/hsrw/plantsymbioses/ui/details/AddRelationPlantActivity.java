package com.hsrw.plantsymbioses.ui.details;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hsrw.plantsymbioses.BaseActivity;
import com.hsrw.plantsymbioses.R;
import com.hsrw.plantsymbioses.constant.FirebaseConstant;
import com.hsrw.plantsymbioses.databinding.ActivityAddRelationBinding;
import com.hsrw.plantsymbioses.model.MainPlant;
import com.hsrw.plantsymbioses.model.PlantSymbioses;
import com.hsrw.plantsymbioses.repo.PlantRepo;
import com.hsrw.plantsymbioses.ui.main.MainActivity;

import java.util.ArrayList;
import java.util.List;

import static com.hsrw.plantsymbioses.constant.FirebaseConstant.COMMENSALISTIC;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.COMPETITION;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.MUTUALISTIC;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.PARASITIC;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.PLANTS_TABLE;
import static com.hsrw.plantsymbioses.ui.main.MainAdapter.KEY_PLANT_MODEL;

public class AddRelationPlantActivity extends BaseActivity {

    ActivityAddRelationBinding binding;
    private DatabaseReference databaseUser;
    List<String> plantNameList = new ArrayList<>();
    List<MainPlant> plantList;
    MainPlant mainPlant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddRelationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mainPlant = getIntent().getExtras().getParcelable(KEY_PLANT_MODEL);

        init();
        getPlantList();
        actionAdd();
    }

    private void actionAdd() {
        binding.btnAdd.setOnClickListener(view -> {
            if (isValidView()) {
                addPlant(plantList);
            }

        });
    }

    private void getPlantList() {
        PlantRepo.getInstance().findAllSingle(plantList -> {
            this.plantList.addAll(plantList);
            for (int i = 0; i < plantList.size(); i++) {
                plantNameList.add(plantList.get(i).getName());
            }
            ArrayAdapter<String> planNameArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, plantNameList);
            planNameArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            binding.spPlantName.setAdapter(planNameArrayAdapter);
        });
    }

    private boolean isValidView() {

        if (binding.etDesc.getText().toString().equals("")) {
            Toast.makeText(this, "Please fill Desc", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    private void init() {
        databaseUser = FirebaseDatabase.getInstance().getReference().child(PLANTS_TABLE);
        plantList = new ArrayList<>();
    }

    private void addPlant(List<MainPlant> plantList) {

        PlantSymbioses plantSymbioses = new PlantSymbioses();
        plantSymbioses.setId(getRelationType().size());
        plantSymbioses.setDescription(binding.etDesc.getText().toString());
        plantSymbioses.setName(plantList.get(binding.spPlantName.getSelectedItemPosition()).getName());
        plantSymbioses.setImage(plantList.get(binding.spPlantName.getSelectedItemPosition()).getImage());

        getRelationType().add(plantSymbioses);

        databaseUser.setValue(plantList);
        Toast.makeText(AddRelationPlantActivity.this, "Added", Toast.LENGTH_SHORT).show();
        finish();


    }


    private List<PlantSymbioses> getRelationType() {

        MainPlant tempMainPlant = plantList.get(mainPlant.getId());
        switch (binding.spRelationType.getSelectedItemPosition() + 1) {
            case MUTUALISTIC:
                return tempMainPlant.getMutualisticList();
            case PARASITIC:
                return tempMainPlant.getParasiticList();
            case COMPETITION:
                return tempMainPlant.getCompetitionList();
            case COMMENSALISTIC:
                return tempMainPlant.getCommensalisticList();
            default:
                return null;
        }
    }


}