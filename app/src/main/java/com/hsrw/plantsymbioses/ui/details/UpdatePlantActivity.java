package com.hsrw.plantsymbioses.ui.details;

import android.os.Bundle;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hsrw.plantsymbioses.databinding.ActivityEditPlantBinding;
import com.hsrw.plantsymbioses.model.MainPlant;
import com.hsrw.plantsymbioses.util.BindingUtils;

import static com.hsrw.plantsymbioses.constant.FirebaseConstant.PLANTS_TABLE;
import static com.hsrw.plantsymbioses.ui.main.MainAdapter.KEY_PLANT_MODEL;

public class UpdatePlantActivity extends AppCompatActivity {

    ActivityEditPlantBinding binding;

    private MainPlant mainPlant;
    private DatabaseReference databaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditPlantBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mainPlant = getIntent().getExtras().getParcelable(KEY_PLANT_MODEL);
        init();
        binding.etName.setText(mainPlant.getName());
        binding.etDesc.setText(mainPlant.getDescription());
        binding.etMaxProd.setText(mainPlant.getMaxProduction());
        binding.etFavorableLight.setText(mainPlant.getFavorableLight());
        binding.spinnerSoilType.setSelection(getIndex(binding.spinnerSoilType, mainPlant.getSoilType()));
        BindingUtils.setImage(binding.ivPlant, mainPlant.getImage());

        binding.btnUpdate.setOnClickListener(view -> {
            update();
        });
    }

    private void update() {
        MainPlant plant = new MainPlant();
        plant.setId(mainPlant.getId());
        plant.setName(binding.etName.getText().toString());
        plant.setDescription(binding.etDesc.getText().toString());
        plant.setFavorableLight(binding.etFavorableLight.getText().toString());
        plant.setImage(mainPlant.getImage());
        plant.setMaxProduction(binding.etMaxProd.getText().toString());
        plant.setSoilType(binding.spinnerSoilType.getSelectedItem().toString());

        databaseUser.child(mainPlant.getId() + "").setValue(plant);
        finish();
        Toast.makeText(this, "Updated", Toast.LENGTH_SHORT).show();
    }

    private int getIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }
        return 0;
    }

    private void init() {
        databaseUser = FirebaseDatabase.getInstance().getReference().child(PLANTS_TABLE);

    }


}