package com.hsrw.plantsymbioses.ui.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hsrw.plantsymbioses.databinding.FragmentRelationPlantBinding;
import com.hsrw.plantsymbioses.model.MainPlant;
import com.hsrw.plantsymbioses.model.PlantSymbioses;
import com.hsrw.plantsymbioses.repo.PlantRepo;
import com.hsrw.plantsymbioses.ui.main.SuggestionsAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.hsrw.plantsymbioses.constant.FirebaseConstant.COMMENSALISTIC;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.COMPETITION;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.MUTUALISTIC;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.PARASITIC;
import static com.hsrw.plantsymbioses.util.SymbiosesPlantUtil.getSymbiosesListString;


public class RelationFragment extends Fragment {


    FragmentRelationPlantBinding binding;
    private List<PlantSymbioses> plantSymbiosesList = new ArrayList<>();
    private SuggestionsAdapter relationAdapter;
    MainPlant mainPlant;
    int positionType;

    public RelationFragment(int position, MainPlant mainPlant) {
        this.mainPlant = mainPlant;
        this.positionType = position;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentRelationPlantBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fillPlantSymbiosesList();

        init();
        getPlants();
        binding.swipeRefreshPlants.setOnRefreshListener(this::getPlants);

    }

    private void fillPlantSymbiosesList() {
        plantSymbiosesList.clear();

        switch (positionType) {
            case MUTUALISTIC:
                plantSymbiosesList.addAll(mainPlant.getMutualisticList());
                break;
            case PARASITIC:
                plantSymbiosesList.addAll(mainPlant.getParasiticList());
                break;
            case COMPETITION:
                plantSymbiosesList.addAll(mainPlant.getCompetitionList());
                break;
            case COMMENSALISTIC:
                plantSymbiosesList.addAll(mainPlant.getCommensalisticList());
                break;
        }
    }

    private void getPlants() {
        PlantRepo.getInstance().findByID(mainPlant.getId(), mainPlant -> {

            binding.swipeRefreshPlants.setRefreshing(false);
            this.mainPlant = mainPlant;
            fillPlantSymbiosesList();
            binding.rvPlants.getAdapter().notifyDataSetChanged();
        });
    }


    private void init() {
        relationAdapter = new SuggestionsAdapter(mainPlant.getId(), getSymbiosesListString(positionType), plantSymbiosesList);
        binding.rvPlants.setAdapter(relationAdapter);

    }


}