package com.hsrw.plantsymbioses.ui.details;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.tabs.TabLayoutMediator;
import com.hsrw.plantsymbioses.BaseActivity;
import com.hsrw.plantsymbioses.R;
import com.hsrw.plantsymbioses.constant.FirebaseConstant;
import com.hsrw.plantsymbioses.databinding.ActivityPlantDetailsBinding;
import com.hsrw.plantsymbioses.model.MainPlant;
import com.hsrw.plantsymbioses.repo.PlantRepo;
import com.hsrw.plantsymbioses.util.BindingUtils;

import static com.hsrw.plantsymbioses.ui.main.MainAdapter.KEY_PLANT_MODEL;

public class PlantDetailsActivity extends BaseActivity {

    ActivityPlantDetailsBinding binding;
    private String[] tabTitles;
    private ViewPagerAdapter viewPagerAdapter;
    private MainPlant mainPlant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPlantDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mainPlant = getIntent().getExtras().getParcelable(KEY_PLANT_MODEL);
        init();
        setSupportActionBar(binding.mToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        activateImageVideoFragment();
        binding.tvName.setText(mainPlant.getName());
        binding.tvDesc.setText(mainPlant.getDescription());
        BindingUtils.setImage(binding.ivCover, mainPlant.getImage());

        PlantRepo.getInstance().findByID(mainPlant.getId(),plant -> {
            binding.tvName.setText(plant.getName());
            binding.tvDesc.setText(plant.getDescription());
        });

    }

    private void init() {
        tabTitles = getResources().getStringArray(R.array.relationPlantTabs);
        viewPagerAdapter = new ViewPagerAdapter(this, tabTitles.length, mainPlant);

    }

    private void activateImageVideoFragment() {
        binding.viewPager.setAdapter(viewPagerAdapter);

        new TabLayoutMediator(binding.tabLayout, binding.viewPager, (tab, position) -> {
            tab.setText(tabTitles[position]);

        }).attach();

        binding.viewPager.setOffscreenPageLimit(4);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_details_plant, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.action_add) {
            Intent intent = new Intent(this, AddRelationPlantActivity.class);
            intent.putExtra(FirebaseConstant.KEY_PLANT_MODEL, mainPlant);
            startActivity(intent);
            return true;
        }
        if (item.getItemId() == R.id.action_update) {
            Intent intent = new Intent(this, UpdatePlantActivity.class);
            intent.putExtra(FirebaseConstant.KEY_PLANT_MODEL, mainPlant);
            startActivity(intent);
            return true;
        }
        if (item.getItemId() == R.id.action_delete) {
            removePlant(mainPlant.getId());

            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    private void removePlant(int id) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure you want to delete this relation?");

        alertDialogBuilder.setPositiveButton("Yes", (arg0, arg1) -> {
                    PlantRepo.getInstance().delete(id, i -> {
                        Toast.makeText(this, "Deleted Successfully.", Toast.LENGTH_SHORT).show();
                        finish();
                    });
                }
        );

        alertDialogBuilder.setNegativeButton("No", (dialog, which) -> {
            Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();

        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }


}