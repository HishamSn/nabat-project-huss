package com.hsrw.plantsymbioses.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hsrw.plantsymbioses.BaseActivity;
import com.hsrw.plantsymbioses.databinding.ActivityAddPlantBinding;
import com.hsrw.plantsymbioses.model.MainPlant;
import com.hsrw.plantsymbioses.repo.Function;
import com.hsrw.plantsymbioses.repo.PlantRepo;

import java.util.List;

import static com.hsrw.plantsymbioses.constant.FirebaseConstant.PLANTS_TABLE;
import static com.hsrw.plantsymbioses.util.IntentUtil.PICK_IMAGE_REQUEST;
import static com.hsrw.plantsymbioses.util.IntentUtil.chooseImageGallery;

public class AddPlantActivity extends BaseActivity {


    StorageReference mStorageReference;
    private Uri filePath;

    ActivityAddPlantBinding binding;
    private DatabaseReference databaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddPlantBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        init();
        binding.ivProfile.setOnClickListener(view -> chooseImageGallery(AddPlantActivity.this));

        binding.btnAdd.setOnClickListener(view -> {

            PlantRepo.getInstance().findAllSingle(plantList -> {
                if (isValidView() && isUnique(plantList)) {
                    addPlant(plantList);
                }

            });
        });
    }

    private boolean isUnique(List<MainPlant> plantList) {

        for (MainPlant mainPlant : plantList) {
            if (mainPlant.getName().equalsIgnoreCase(binding.etName.getText().toString())) {
                Toast.makeText(this, "This Plant Already Exist", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    private boolean isValidView() {

        if (filePath == null) {
            Toast.makeText(this, "Please Insert The photo", Toast.LENGTH_SHORT).show();
            return false;
        } else if (binding.etName.getText().toString().equals("")) {
            Toast.makeText(this, "Please fill name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (binding.etDesc.getText().toString().equals("")) {
            Toast.makeText(this, "Please fill desc", Toast.LENGTH_SHORT).show();
            return false;

        } else if (binding.etFavorableLight.getText().toString().equals("")) {
            Toast.makeText(this, "Please favorable light ", Toast.LENGTH_SHORT).show();
            return false;

        } else if (binding.etMaxProd.getText().toString().equals("")) {
            Toast.makeText(this, "Please max prod ", Toast.LENGTH_SHORT).show();
            return false;

        }

        return true;
    }


    private void init() {
        mStorageReference = FirebaseStorage.getInstance().getReference();
        databaseUser = FirebaseDatabase.getInstance().getReference().child(PLANTS_TABLE);

    }

    private void addPlant(List<MainPlant> plantList) {

        upLoadPhotoToFirebase(plantList.size(), plantsImageUrl -> {
            MainPlant plant = new MainPlant();
            plant.setId(plantList.size());
            plant.setName(binding.etName.getText().toString());
            plant.setDescription(binding.etDesc.getText().toString());
            plant.setFavorableLight(binding.etFavorableLight.getText().toString());
            plant.setImage(plantsImageUrl);
            plant.setMaxProduction(binding.etMaxProd.getText().toString());
            plant.setSoilType(binding.spinnerSoilType.getSelectedItem().toString());

            plantList.add(plant);
            databaseUser.setValue(plantList);
            Toast.makeText(AddPlantActivity.this, "Added", Toast.LENGTH_SHORT).show();
            finish();
        });


    }

    private void upLoadPhotoToFirebase(int id, Function<String> function) {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("uploading ..");
            progressDialog.show();

            StorageReference storageRef = mStorageReference.child("plants/" + id);

            UploadTask uploadTask = storageRef.putFile(filePath);
            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    progressDialog.dismiss();
                    Toast.makeText(AddPlantActivity.this, "Failed " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    throw task.getException();
                }
                return storageRef.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    function.done(String.valueOf(downloadUri));


                    progressDialog.dismiss();
                    Toast.makeText(AddPlantActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                    finish();

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(AddPlantActivity.this, "Failed " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {
            filePath = data.getData();
            try {

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Glide.with(AddPlantActivity.this)
                        .load(bitmap)
                        .into(binding.ivProfile);


            } catch (Exception e) {
                Toast.makeText(AddPlantActivity.this, "Failed onActivityResult" + e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    }
}