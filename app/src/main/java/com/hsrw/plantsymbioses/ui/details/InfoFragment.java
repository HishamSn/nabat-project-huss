package com.hsrw.plantsymbioses.ui.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hsrw.plantsymbioses.databinding.FragmentInfoPlantBinding;
import com.hsrw.plantsymbioses.databinding.FragmentRelationPlantBinding;
import com.hsrw.plantsymbioses.model.MainPlant;


public class InfoFragment extends Fragment {

    MainPlant mainPlant;
    FragmentInfoPlantBinding binding;

    public InfoFragment(MainPlant mainPlant) {
        this.mainPlant = mainPlant;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentInfoPlantBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setPlant(mainPlant);


    }


}