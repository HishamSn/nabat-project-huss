package com.hsrw.plantsymbioses.ui.details;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.hsrw.plantsymbioses.model.MainPlant;


public class ViewPagerAdapter extends FragmentStateAdapter {

    private int arraySize;
    private MainPlant mainPlant;

    public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity, int length, MainPlant mainPlant) {
        super(fragmentActivity);
        this.arraySize = length;
        this.mainPlant = mainPlant;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        if (position == 0) {
            return new InfoFragment(mainPlant);
        }
        return new RelationFragment(position, mainPlant);
    }

    @Override
    public int getItemCount() {
        return arraySize;

    }
}
