package com.hsrw.plantsymbioses.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.hsrw.plantsymbioses.R;
import com.hsrw.plantsymbioses.databinding.RowPlantSuggBinding;
import com.hsrw.plantsymbioses.model.PlantSymbioses;
import com.hsrw.plantsymbioses.repo.PlantRepo;

import java.util.ArrayList;
import java.util.List;

public class SuggestionsAdapter extends RecyclerView.Adapter<SuggestionsAdapter.ViewHolder> {
    public static String KEY_PLANT_MODEL = "MAIN_PLANT";
    private static final int ROW_PLANT = R.layout.row_plant;
    private List<PlantSymbioses> plantSymbiosesList = new ArrayList<>();
    private Context context;
    String symbiosesListString;
    int plantId;

    public SuggestionsAdapter(int plantId,String symbiosesListString, List<PlantSymbioses> plantSymbiosesList) {
        this.plantSymbiosesList = plantSymbiosesList;
        this.symbiosesListString = symbiosesListString;
        this.plantId = plantId;

    }

    public SuggestionsAdapter(List<PlantSymbioses> plantSymbiosesList) {
        this.plantSymbiosesList = plantSymbiosesList;


    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public int getItemViewType(int position) {
        return ROW_PLANT;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowPlantSuggBinding binding = RowPlantSuggBinding.inflate(layoutInflater, parent, false);

        return new ViewHolder(binding);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        PlantSymbioses plantSymbioses = plantSymbiosesList.get(position);
        holder.bind(plantSymbioses);

        if (symbiosesListString == null) {
            return;
        }
        holder.itemView.setOnLongClickListener(view -> {
            removePlant(plantSymbioses.getId());
            return true;
        });
    }

    private void removePlant(int id) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Are you sure you want to delete this relation?");

        alertDialogBuilder.setPositiveButton("Yes", (arg0, arg1) -> {
                    PlantRepo.getInstance().deleteRelation(plantId,symbiosesListString, id, i -> {
                        notifyItemRemoved(id);
                        Toast.makeText(context, "Deleted Successfully.", Toast.LENGTH_SHORT).show();
                        notifyDataSetChanged();
                    });
                }
        );

        alertDialogBuilder.setNegativeButton("No", (dialog, which) -> {
            Toast.makeText(context, "Cancel", Toast.LENGTH_SHORT).show();

        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public int getItemCount() {
        return plantSymbiosesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RowPlantSuggBinding binding;

        ViewHolder(RowPlantSuggBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(PlantSymbioses plantSymbioses) {
            binding.setPlantSymbioses(plantSymbioses);
            binding.executePendingBindings();
        }
    }
}
