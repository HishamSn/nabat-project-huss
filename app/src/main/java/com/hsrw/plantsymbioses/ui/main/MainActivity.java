package com.hsrw.plantsymbioses.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.hsrw.plantsymbioses.BaseActivity;
import com.hsrw.plantsymbioses.databinding.ActivityMainBinding;
import com.hsrw.plantsymbioses.model.MainPlant;
import com.hsrw.plantsymbioses.model.PlantSymbioses;
import com.hsrw.plantsymbioses.repo.PlantRepo;
import com.hsrw.plantsymbioses.ui.AddPlantActivity;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity {


    ActivityMainBinding binding;
    private MainAdapter mainAdapter;
    private SuggestionsAdapter suggestionAdapter;
    private List<MainPlant> plantList;
    private List<PlantSymbioses> suggestionList;
    ArrayList<MainPlant> filteredList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
        binding.fabAddPlant.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, AddPlantActivity.class));
        });

//        setUpSearchBox();
        binding.swipeRefreshPlants.setOnRefreshListener(this::getPlants);


        searchAction();
    }

    @Override
    protected void onResume() {
        getPlants();
        super.onResume();
    }

    private void searchAction() {
        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void filter(String data) {
        filteredList.clear();
        for (MainPlant item : plantList) {
            if (item.getName().toLowerCase().contains(data.toLowerCase())) {
                filteredList.add(item);
            }
        }


        mainAdapter.filterList(filteredList);

        suggestionList.clear();
        if (filteredList.size() < plantList.size() && !filteredList.isEmpty()) {

            for (MainPlant mainPlant : filteredList) {
                suggestionList.addAll(mainPlant.getMutualisticList());
            }

            if (!suggestionList.isEmpty()) {
                binding.llSuggestions.setVisibility(View.VISIBLE);
                binding.rvSuggestions.setVisibility(View.VISIBLE);
            }

        } else {
            binding.llSuggestions.setVisibility(View.GONE);
            binding.rvSuggestions.setVisibility(View.GONE);
        }
        suggestionAdapter.notifyDataSetChanged();
    }

    private void getPlants() {
        PlantRepo.getInstance().findAllSingle(plantList -> {
            binding.swipeRefreshPlants.setRefreshing(false);
            this.plantList.clear();
            this.plantList.addAll(plantList);
            mainAdapter.notifyDataSetChanged();
        });
    }

    private void init() {
        plantList = new ArrayList<>();
        suggestionList = new ArrayList<>();
        filteredList = new ArrayList<>();
        mainAdapter = new MainAdapter(plantList);
        suggestionAdapter = new SuggestionsAdapter(suggestionList);
        binding.rvPlants.setAdapter(mainAdapter);
        binding.rvSuggestions.setAdapter(suggestionAdapter);
    }


    @Override
    public void onBackPressed() {
        if (binding.etSearch.getText().toString().length() > 0) {
            binding.etSearch.setText("");
            return;
        }
        super.onBackPressed();
    }
}