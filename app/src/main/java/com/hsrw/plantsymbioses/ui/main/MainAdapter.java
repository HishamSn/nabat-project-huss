package com.hsrw.plantsymbioses.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hsrw.plantsymbioses.R;
import com.hsrw.plantsymbioses.databinding.RowPlantBinding;
import com.hsrw.plantsymbioses.repo.PlantRepo;
import com.hsrw.plantsymbioses.ui.details.PlantDetailsActivity;
import com.hsrw.plantsymbioses.model.MainPlant;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    public static String KEY_PLANT_MODEL = "MAIN_PLANT";
    private static final int ROW_PLANT = R.layout.row_plant;
    private List<MainPlant> plantList = new ArrayList<>();
    private Context context;

    public MainAdapter(List<MainPlant> plantList) {
        this.plantList = plantList;

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public int getItemViewType(int position) {
        return ROW_PLANT;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowPlantBinding binding = RowPlantBinding.inflate(layoutInflater, parent, false);

        return new ViewHolder(binding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        MainPlant plant = plantList.get(position);
        holder.bind(plant);
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, PlantDetailsActivity.class);
            intent.putExtra(KEY_PLANT_MODEL, plant);
            context.startActivity(intent);

        });


    }

    public void filterList(ArrayList<MainPlant> filteredList) {
        plantList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return plantList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RowPlantBinding binding;

        ViewHolder(RowPlantBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(MainPlant mainPlant) {
            binding.setPlant(mainPlant);
            binding.executePendingBindings();
        }
    }
}
