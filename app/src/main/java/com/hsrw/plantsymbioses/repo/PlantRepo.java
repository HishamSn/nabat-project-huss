package com.hsrw.plantsymbioses.repo;


import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hsrw.plantsymbioses.model.MainPlant;
import com.hsrw.plantsymbioses.model.PlantSymbioses;

import java.util.ArrayList;
import java.util.List;

import static com.hsrw.plantsymbioses.constant.FirebaseConstant.PLANTS_TABLE;


public class PlantRepo {

    private static PlantRepo instance;
    DatabaseReference databasePlant;

    PlantRepo() {
        databasePlant = FirebaseDatabase.getInstance().getReference().child(PLANTS_TABLE);
    }

    public static synchronized PlantRepo getInstance() {
        if (instance == null) {
            instance = new PlantRepo();
        }
        return instance;
    }


    public void insertPlant(MainPlant plant, Function<List<MainPlant>> function) {

        List<MainPlant> plantList = new ArrayList<>();

        databasePlant.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                plantList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    MainPlant ads = child.getValue(MainPlant.class);
                    plantList.add(ads);
                }
                function.done(plantList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    public void findAllSingle(Function<List<MainPlant>> function) {

        List<MainPlant> plantList = new ArrayList<>();

        databasePlant.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                plantList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    MainPlant ads = child.getValue(MainPlant.class);
                    plantList.add(ads);
                }
                function.done(plantList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void findByID(int id, Function<MainPlant> function) {


        databasePlant.child(id + "").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                MainPlant plant = dataSnapshot.getValue(MainPlant.class);
                function.done(plant);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    public void delete(int id, Function<Integer> function) {
        databasePlant.child(String.valueOf(id)).removeValue();
        function.done(id);

    }

    public void deleteRelation(int plantId,String PlantSymbioses, int relationId, Function<Integer> function) {
        databasePlant.child(String.valueOf(plantId))
                .child(PlantSymbioses)
                .child(String.valueOf(relationId))
                .removeValue().addOnCompleteListener(task -> function.done(relationId));

    }

    public void findAllRunTime(Function<List<MainPlant>> function) {

        List<MainPlant> plantList = new ArrayList<>();

        databasePlant.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                plantList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    MainPlant ads = child.getValue(MainPlant.class);
                    plantList.add(ads);
                }
                function.done(plantList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
