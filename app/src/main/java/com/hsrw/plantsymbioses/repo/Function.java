package com.hsrw.plantsymbioses.repo;


public interface Function<T> {
    void done(T t);
}
