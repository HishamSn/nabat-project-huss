package com.hsrw.plantsymbioses.constant;

public final class FirebaseConstant {


    public static final String PLANTS_TABLE = "Plants";


    public static String KEY_PLANT_MODEL = "MAIN_PLANT";

    public static final int MUTUALISTIC = 1;
    public static final int PARASITIC = 2;
    public static final int COMPETITION = 3;
    public static final int COMMENSALISTIC = 4;
}
