package com.hsrw.plantsymbioses.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class IntentUtil {
    public static final int PICK_IMAGE_REQUEST = 71;

    public static void chooseImageGallery(Activity activity) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
}
