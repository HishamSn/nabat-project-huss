package com.hsrw.plantsymbioses.util;

import static com.hsrw.plantsymbioses.constant.FirebaseConstant.COMMENSALISTIC;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.COMPETITION;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.MUTUALISTIC;
import static com.hsrw.plantsymbioses.constant.FirebaseConstant.PARASITIC;

public class SymbiosesPlantUtil {


    public static String getSymbiosesListString(int positionType) {

        switch (positionType) {
            case MUTUALISTIC:
                return "mutualisticList";
            case PARASITIC:
                return "parasiticList";
            case COMPETITION:
                return "competitionList";
            case COMMENSALISTIC:
                return "commensalisticList";
        }
        return null;
    }

}
