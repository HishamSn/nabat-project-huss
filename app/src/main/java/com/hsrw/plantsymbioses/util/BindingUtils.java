package com.hsrw.plantsymbioses.util;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.hsrw.plantsymbioses.R;

public class BindingUtils {

    public BindingUtils() {
    }

    @BindingAdapter("url")
    public static void setImage(ImageView image, String url) {

        Glide.with(image.getContext()).load(url).placeholder(R.drawable.ic_baseline_image)
                .error(R.drawable.ic_baseline_error).into(image);

//        Picasso.get().load(url).fit().centerCrop()
//                .placeholder(R.drawable.add_icon)
//                .error(R.drawable.ic_launcher_background).into(image);


    }
}
